// rafc

import React from 'react'

export const NotFound = () => {
    return <div className="fw-bold fs-1 text-center"> 404 NotFound</div>
}
