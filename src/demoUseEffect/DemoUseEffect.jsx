// rafc
import React, { useEffect, useState, useCallback, useMemo, memo, useRef, useContext } from 'react'
import { Child } from './Child'

export const DemoUseEffect = () => {
    // useEffect(callback, dependencies)
    // Tham số callback: bắt buộc.
    // tham số dependencies: Có thể có hoặc ko
    // Callback chỉ đc thực thị khi component đc render xong. (tất cả các trường hợp của useEffect)
    // useEffect: Luôn luôn thực thi ít nhất là 1 lần

    // Note: useEffect ở trong những component con luôn luôn đc thực thi trước những useEffect ở trong compontn cha chứa nó

    const [number, setNumber] = useState(1)

    const [like, setLike] = useState(1)

    // TH1: useEffect sẽ đc thực thi lại mỗi khi component render lại
    useEffect(() => {
        console.log('useEffect TH1')
    })

    // TH2: useEffect chỉ thực thi duy nhất 1 lần
    useEffect(() => {
        console.log('useEffect TH2')
    }, [])

    // TH3: giá trị trong mảng dependencies thay đổi useEffect sẽ đc thực thi lại
    useEffect(() => {
        console.log('useEffect TH3')
    }, [number])

    // TH4: clean up

    useEffect(() => {
        console.log('useEffect TH4')

        const interval = setInterval(() => {
            console.log('hello BC69')
        }, 1000)

        return () => {
            //  chỉ thực thi khi component bị unmount (bị hủy khỏi UI, bị xóa khỏi DOM)
            console.log('useEffect TH4 return')
            clearInterval(interval)
        }
    }, [])

    console.log('RENDER')
    return (
        <div>
            <h1>Demo UseEffect</h1>
            <p className="fs-2 fw-bold mb-3">Number: {number}</p>
            <button
                className="btn btn-success"
                onClick={() => {
                    setNumber(number + 1)
                }}
            >
                + Number
            </button>

            <p className="fs-2 fw-bold my-3">Like: {like}</p>
            <button
                className="btn btn-success"
                onClick={() => {
                    setLike(like + 1)
                }}
            >
                + Like
            </button>

            <Child />
        </div>
    )
}
