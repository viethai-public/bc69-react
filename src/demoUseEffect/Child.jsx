// rafc
import React, { useEffect } from 'react'

export const Child = () => {
    useEffect(() => {
        console.log('useEffect child')
    }, [])
    return <div>Child</div>
}
