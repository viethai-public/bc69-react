// rafc
import React, {
    useMemo,
    useState,
    useCallback,
    memo,
    useRef,
    useContext,
    useId,
    useTransition,
} from 'react'
import Child from './Child'

export const DemoHook = () => {
    const [number, setNumber] = useState(1)
    const [like, setLike] = useState(1)

    let color = {
        red: 'red',
    } // #1234

    // #12345

    //  kết hợp vs việc sử dụng memo ở component nhận props
    const sum = useCallback(() => {
        console.log('SUM FUNC')
        console.log(number)
    }, []) //#1111

    const total = useMemo(() => {
        console.log('useMemo')
        let total = 0
        for (let i = 1; i <= number; i++) {
            total += i
        }
        return total
    }, [number])

    // const sum = (n) => {
    //     let total = 0
    //     for (let i = 1; i <= n; i++) {
    //         total += i
    //     }
    //     return total
    // }

    // const total = sum(10000000)

    // useRef: Mọi giá trị lấy thông qua thuộc tính current
    // Lưu giá trị
    let obj = {}
    let objRef = useRef({})

    // DOM
    const inputRef = useRef(null)

    console.log('obj: ', obj)
    console.log('objRef: ', objRef.current)

    console.log('total: ', total)
    console.log('RENDER')
    return (
        <div>
            <h1>Demo Hook</h1>

            <div className="mt-3">
                <p className="fs-2">Number: {number}</p>
                <button
                    className="btn btn-success mt-3"
                    onClick={() => {
                        obj = { name: 'abc' }
                        objRef.current = { name: 'abc' }
                        setNumber(number + 1)
                    }}
                >
                    + Number
                </button>
            </div>

            <input type="text" className="form-control mt-5" ref={inputRef} />

            <button
                className="btn btn-danger"
                onClick={() => {
                    const input = inputRef.current
                    console.log('input: ', input)
                    console.log(input.value)
                }}
            >
                Get Value Input
            </button>

            <Child sum={sum} />
        </div>
    )
}
