// rafc

// HOF: higher oreder function
// closure func

// HOC: higher order component

import React, { memo } from 'react'

const Child = () => {
    console.log('Child RENDER')
    return <div>Child</div>
}

export default memo(Child)
